const gulp = require('gulp');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const del = require('del');
const imagemin = require('gulp-imagemin');
const browserSync = require('browser-sync').create();

function styles(){
	return gulp.src('./app/**/*.css')
				.pipe(concat('all.css'))
				.pipe(autoprefixer({
            browsers: ['> 0.01%'],
            cascade: false
        }))
        .pipe(cleanCSS({level: 2}))
				.pipe(gulp.dest('./dist/css'))
				.pipe(browserSync.stream());
}

function scripts(){
	return gulp.src(['./app/js/0_carousel.js', './app/js/1_jquery.js','./app/js/2_scroll.js','./app/js/3_video.js','./app/js/4_js.js'])
				.pipe(concat('all.js'))
				.pipe(uglify({toplevel: true}))
				.pipe(gulp.dest('./dist/js'))
				.pipe(browserSync.stream());
}

function img(){
	return gulp.src('./app/img/*')
				.pipe(imagemin())
        .pipe(gulp.dest('dist/images/*'))
				.pipe(browserSync.stream());
}
function html(){
	return gulp.src('./*.html')
	.pipe(browserSync.stream());
}

function watch(){
	browserSync.init({
        server: {
            baseDir: "./"
        }
        // tunnel: true
    });
	gulp.watch('./app/**/*.css', styles);
	gulp.watch('./app/**/*.js', scripts);
	gulp.watch('./*.html',  html);
}


function clean(){
	return del(['dist/*'])
}

gulp.task('styles', styles);
gulp.task('scripts', scripts);
gulp.task('img', img);
gulp.task('watch', watch);

gulp.task('f', gulp.series(clean, 
		gulp.parallel(styles, scripts)
));