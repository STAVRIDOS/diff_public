if(window.innerWidth >= 767){
  var video = document.querySelector(".video");
      function onYouTubeIframeAPIReady() {
          player = new YT.Player('player', {
              height: '100%',
              width: '100%',
              videoId: videoId,
              modestbranding: 1,
              events: {
                  'onStateChange': onPlayerStateChange
              }
          });
      };

      function onPlayerStateChange(event) {
          if (event.data == YT.PlayerState.PLAYING && !done) {
              done = true;
          }
      }

    setTimeout(function(){
          video.addEventListener("click", customVideo );

          function customVideo(e){
            var imgPlay = document.querySelector(".play"),
            vid = video.querySelector("#player");
            if(e.target === vid || e.target.parentNode === imgPlay){
              imgPlay.classList.add("playOn");
            (vid.requestFullscreen) ? vid.requestFullscreen() :
            (vid.mozRequestFullScreen) ? vid.mozRequestFullScreen() :
            (vid.webkitRequestFullscreen) ? vid.webkitRequestFullscreen() :
            (vid.msRequestFullscreen) ? vid.msRequestFullscreen() : "";
             player.playVideo();
            }
          }
      }, 1000)
}