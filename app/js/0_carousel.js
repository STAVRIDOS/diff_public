var row = document.querySelector(".row-all"),
	sl = row.querySelectorAll(".sl"),
	count,
	step,
	steps = [],
	sliders,
	slider,
	blocksAll,
	blockWidth,
	colBlock,
	active_link,
	circle,
	num = 0,
	nums = [],
	indexBlock = 0,
	indexArray;
row.addEventListener("click", click);
(function() {
	for (var b = 0; b < sl.length; b += 1) {
		var rs = sl[b].querySelector('.row-slider');
		var num = rs.querySelectorAll('.myslide');
		colBlock = num.length;
		var count = (window.innerWidth < 767) ? 1 : +rs.dataset.col;
		navigations(sl[b], num);
		createCircle(sl[b], colBlock, count);
		sl[b].setAttribute("data-number", b)
		steps.push(0)
		nums.push(0)
		var row_block = sl[b];
		var lb = row_block.querySelector(".leftButton");
		var rb = row_block.querySelector(".rightButton");
		var cirR = row_block.querySelector(".block-circle");
		var cir = cirR.querySelectorAll("span");
		if (cir.length < 2) {
			rb.setAttribute("style", "display: none");
			lb.setAttribute("style", "display: none");
			cirR.setAttribute("style", "height:13px");
			cirR.innerHTML = "";
		}
	}
}())

function click(e) {
	row_block = e.target.closest(".sl");
	if (row_block !== null && row_block !== undefined) {
		(row_block.hasAttribute("data-number")) ? indexArray = row_block.dataset.number: "";
		step = steps[indexArray];
		cirCol = row_block.querySelectorAll(".circle");
		if (row_block.querySelector('.active-link')) {
			block = row_block.querySelector(row_block.querySelector('.active-link').getAttribute('href'));
		} else {
			block = row_block.querySelector('.slider');
		}
		slider = block.querySelector(".row-slider");
		(window.innerWidth < 767) ? count = 1: count = colSlider();
		blockWidth = widthOneBlock(block);
		colBlock = colBlocks(block);
		num = nums[indexArray];
		var evClick;
		(window.innerWidth < 767) ? evClick = e.target.getAttribute("class"): evClick = e.target.parentNode.getAttribute("class");
		if (e.target.getAttribute("class") === "rightButton") {
			slideRight(slider, blockWidth, colBlock, indexArray, row_block);
		} else if (e.target.getAttribute("class") === "leftButton") {
			slideLeft(slider, blockWidth, colBlock, indexArray, row_block);
		} else if (evClick === "buttons-navigations") {
			circle = row_block.querySelectorAll(".block-circle");
			tab(e);
			(circle.length > 0) ? circle[0].parentNode.removeChild(circle[0]): "";
			createCircle(row_block, colBlock, count);
			nums[indexArray] = 0;
			num = 0;
			activeCircle(num)
		} else if (e.target.parentNode.getAttribute("class") === "block-circle") {
			var round = +e.target.dataset.index;
			if (num <= round) {
				var onr = round - num;
				for (var i = 0; i < onr; i++) {
					slideRight()
				}
			} else {
				var onl = num - round;
				for (var i = 0; i < onl; i++) {
					slideLeft()
				}
			}
			num = round;
			activeCircle()
		}
		steps[indexArray] = step;
	}
}

function tab(e) {
	step = 0;
	slider.style = "margin-left: 0px";
}

function slideLeft() {
	num = num - 1;
	if (step <= 0) {
		step = colBlock * blockWidth - (blockWidth * count);
		var marg = "margin-left:-" + step + "px";
		slider.setAttribute("style", marg)
	} else {
		step -= blockWidth;
		var marg = "margin-left:-" + step + "px";
		slider.setAttribute("style", marg)
	}
	activeCircle(indexArray)
}

function slideRight() {
	num = num + 1;
	if (step < colBlock * blockWidth - (blockWidth * count)) {
		step += blockWidth;
		var marg = "margin-left:-" + step + "px";
		slider.setAttribute("style", marg)
	} else {
		step = 0;
		var marg = "margin-left:-" + step + "px";
		slider.setAttribute("style", marg)
	}
	activeCircle(indexArray)
}

function navigations(appendElement, blocksAll) {
	var blockButton = document.createElement("div");
	var leftButton = document.createElement("span");
	var rightButton = document.createElement("span");
	blockButton.classList.add('block-button');
	leftButton.classList.add('leftButton');
	leftButton.setAttribute("style", "display: none");
	rightButton.classList.add('rightButton');
	blockButton.appendChild(leftButton);
	blockButton.appendChild(rightButton);
	appendElement.appendChild(blockButton);
};

function colBlocks(block) {
	var col = block.querySelectorAll(".myslide").length;
	return col;
}

function colSlider() {
	if (slider.dataset.col != count) {
		step = 0;
		count = slider.dataset.col;
	} else {
		count = slider.dataset.col;
	}
	return count;
}

function widthOneBlock(block) {
	var col = block.querySelectorAll(".myslide");
	return col[0].offsetWidth;
}

function createCircle(appendElement, colBlock, count) {
	var blockCircle = document.createElement("div");
	blockCircle.classList.add('block-circle');
	for (var i = 0; i < colBlock - count + 1; i++) {
		var circleOne = document.createElement("span");
		(i == 0) ? circleOne.classList.add('active-circle'): "";
		circleOne.classList.add('circle')
		circleOne.setAttribute("data-index", i)
		blockCircle.appendChild(circleOne);
	}
	appendElement.appendChild(blockCircle);
}

function activeCircle() {
	if (num >= cirCol.length) {
		num = 0;
	} else if (num < 0) {
		num = cirCol.length - 1;
	}
	var lb = row_block.querySelector(".leftButton");
	var rb = row_block.querySelector(".rightButton");
	var cirR = row_block.querySelector(".block-circle");
	var cir = cirR.querySelectorAll("span");
	if (num === cirCol.length - 1) {
		rb.setAttribute("style", "display: none");
		lb.setAttribute("style", "display: block");
	} else if (num === 0) {
		rb.setAttribute("style", "display: block");
		lb.setAttribute("style", "display: none");
	} else if (num > 0 || num < cirCol.length - 1) {
		rb.setAttribute("style", "display: block");
		lb.setAttribute("style", "display: block");
	}
	if (cir.length < 2) {
		rb.setAttribute("style", "display: none");
		lb.setAttribute("style", "display: none");
		cirR.setAttribute("style", "height:13px");
		cirR.innerHTML = "";
	}
	for (var i = 0; i < cirCol.length; i += 1) {
		if (i == num) {
			cirCol[i].classList.add("active-circle")
		} else {
			cirCol[i].classList.remove("active-circle")
		}
	}
	nums[indexArray] = num;
}

function closestAlt(elem) {
	for (var i = 0;; i++) {
		if (elem.parentNode.classList.contains("sl")) {
			return elem.parentNode;
		} else {
			elem = elem.parentNode;
		}
	}
}